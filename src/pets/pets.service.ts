import { Injectable, HttpException } from '@nestjs/common';
import { PETS } from '../petsdata/pets.sampledata';

@Injectable()
export class PetsService {
	pets = PETS;

      getPets(): Promise<any> {
          return new Promise(resolve => {
              resolve(this.pets);
          });
      }

      addPet(pet): Promise<any> {
        return new Promise(resolve => {
            this.pets.push(pet);
            resolve(this.pets);
        });
    }
    getPet(petID): Promise<any> {
          let id = Number(petID);
          return new Promise(resolve => {
              const pet = this.pets.find(pet => pet.id === id);
              if (!pet) {
                  throw new HttpException('Pet does not exist!', 404);
              }
              resolve(pet);
          });
      }

      deletePet(petID): Promise<any> {
        let id = Number(petID);
        return new Promise(resolve => {
            let index = this.pets.findIndex(pet => pet.id === id);
            if (index === -1) {
                throw new HttpException('pet does not exist!', 404);
            }
            this.pets.splice(1, index);
            resolve(this.pets);
        });
    }

}
