export class CreatePetDTO {
    readonly id: number;
    readonly name: string;
    readonly species: string;
    readonly birthYear: number;
    readonly photo: string;

}