import { Controller, Get, Param, Post, Body, Query, Delete  } from '@nestjs/common';
import { CreatePetDTO } from './dto/create-pet.dto';
import { PetsService } from './pets.service';

@Controller('pets')
export class PetsController {
	constructor(private PetsService: PetsService) { }

    @Get()
    async getPets() {
        const pets = await this.PetsService.getPets();
        return pets;
    }

    @Get(':petID')
    async getPet(@Param('petID') petID) {
        const pet = await this.PetsService.getPet(petID);
        return pet;
    }

    @Post()
    async addPet(@Body() CreatePetDTO: CreatePetDTO) {
        const pet = await this.PetsService.addPet(CreatePetDTO);
        return pet;
    }

    @Delete()
    async deletePet(@Query() query) {
        const pets = await this.PetsService.deletePet(query.petID);
        return pets;
    }
}
